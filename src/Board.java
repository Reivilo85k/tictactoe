import java.util.Scanner;

public class Board {

    private char[][] board;

    public Board(char[][] board) {
        this.board = board;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                board[i][j] = 95;
            }
        }
    }

    public char[][] getBoard() {
        return board;
    }

    public char[][] setBoard(char[][] board) {
        this.board = board;
        return board;
    }

    public static Board createBoard(Board board) {
        char[][] tempBoard = board.getBoard();
        //drawing the board.
        //i = column, j = row
        for (int i = 0; i < tempBoard.length; i++) {
            for (int j = 0; j < tempBoard[i].length; j++) {
                tempBoard[i][j] = 95;
            }
        board.setBoard(tempBoard);
        }return board;
    }
    public static boolean checkCells(String input, Board gameBoard) {
        boolean availableCell = false;
        char[][] tempBoard = gameBoard.getBoard();
        if (input.equals("A0") || input.equals("A1") || input.equals("A2") || input.equals("B0")
                || input.equals("B1") || input.equals("B2") || input.equals("C0") || input.equals("C1")
                || input.equals("C2")) {
            for (int i = 0; i < tempBoard.length; i++) {
                for (int j = 0; j < tempBoard[i].length; j++) {
                    if (tempBoard[i][j] == (char) 95) {
                        availableCell = true;
                    }
                }
            }
        }return availableCell;
    }

    public static Board playerMove(Board gameBoard,Player player){
        Scanner in = new Scanner(System.in);
        for (int x = 0; x < 1;){
            System.out.println("\n\nPlease chose your next move (A1,A2 etc.)");
            String input = in.next();
            if (checkCells(input, gameBoard)) {
                x++;
                switch (input) {
                    case ("A0"):
                        //the loop checks the players do not play on an already played tile
                        gameBoard.setBoard(gameBoard.getBoard())[0][0] = player.getPawn();
                        break;
                    case ("A1"):
                        gameBoard.setBoard(gameBoard.getBoard())[1][0] = player.getPawn();
                        break;
                    case ("A2"):
                        gameBoard.setBoard(gameBoard.getBoard())[2][0] = player.getPawn();
                        break;
                    case ("B0"):
                        gameBoard.setBoard(gameBoard.getBoard())[0][1] = player.getPawn();
                        break;
                    case ("B1"):
                        gameBoard.setBoard(gameBoard.getBoard())[1][1] = player.getPawn();
                        break;
                    case ("B2"):
                        gameBoard.setBoard(gameBoard.getBoard())[2][1] = player.getPawn();
                        break;
                    case ("C0"):
                        gameBoard.setBoard(gameBoard.getBoard())[0][2] = player.getPawn();
                        break;
                    case ("C1"):
                        gameBoard.setBoard(gameBoard.getBoard())[1][2] = player.getPawn();
                        break;
                    case ("C2"):
                        gameBoard.setBoard(gameBoard.getBoard())[2][2] = player.getPawn();
                        break;
                }
            } else {
                System.out.println("Incorrect input, please try again!");
            }
        }
        return gameBoard;
    }

}



