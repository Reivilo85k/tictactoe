import java.util.Random;

public class CoinToss {
    private enum Coin {Heads, Tails}

    Random randomNum = new Random();
    private final int result = randomNum.nextInt(2);
    private final int heads = 0;
    private final int tails = 1;
    Coin coinFlip;

    //randomly chose a first player
    public String flip(){
        if (result == 0){
            coinFlip = Coin.Heads;
            return ("Player 1 starts");
        }else {
            coinFlip = Coin.Tails;
            return ("Player 2 starts");
        }
    }
}
