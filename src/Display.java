public class Display {

    //displays the board
    public static void displayBoard(Board board){
        char[][] tempBoard = board.getBoard();
        System.out.print("\n  A B C");
        //drawing the board.
        //i = column, j = row
        for (int i = 0; i < tempBoard.length; i++) {
            for (int j = 0; j < tempBoard[i].length; j++) {
                if (j == 0) {
                    System.out.print("\n"+ i + " " + tempBoard[i][j]);
                } else{
                    System.out.print(" " + tempBoard[i][j]);
                }
            }
        }
    }

}
