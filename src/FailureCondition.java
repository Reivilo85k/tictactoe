public class FailureCondition {

    //To be called if board is filled with no winner
    public static boolean failure(Board board) {
        char[][] tempBoard = board.getBoard();

        for (int i = 0; i < tempBoard.length; i++) {
            for (int j = 0; j < tempBoard[i].length; j++) {
                if (tempBoard[i][j] == 95) {
                    return false;
                }
            }
        }return true;
    }
}
