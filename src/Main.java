import java.util.Scanner;

//Tic Tac Toe Exercise.
//Create a Tic Tac Toe game using Java console.
//Remember – keep it simple. You may use two-dimensional array to store the
//results and empty fields. Prepare a method to view present state of the “board”.

public class Main {
    public static void main(String[] args) {
        //Create 2 players
        Scanner inputPlayer = new Scanner((System.in));
        System.out.println("Enter Player 1");
        Player player1 = new Player(inputPlayer.nextLine(), (char) 88);
        System.out.println("Enter Player 2");
        Player player2 = new Player(inputPlayer.nextLine(), (char) 79);
        //launch match method in Processing
        Processing.gameMatch(player1,player2);

    }
}
