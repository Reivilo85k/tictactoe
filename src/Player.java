public class Player {
    private String name;
    private char pawn;

    public Player(String name, char pawn) {
        this.name = name;
        this.pawn = pawn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public char getPawn() {
        return pawn;
    }

    public void setPawn(char pawn) {
        this.pawn = pawn;
    }
}
