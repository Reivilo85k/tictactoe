import static jdk.nashorn.internal.objects.NativeString.toUpperCase;

public class Processing {
    public static Board gameMatch(Player player1, Player player2) {
        //Create an empty two-dimensional array and initialize it with the values defined in the createBoard method.
        char[][] board = new char[3][3];
        Board gameBoard = new Board(board);
        gameBoard = Board.createBoard(gameBoard);

        //Randomly chose first player
        CoinToss coinToss = new CoinToss();
        String firstRound = coinToss.flip();
        int turn;
        int round = 0;
        if (firstRound.equals("Player 1 starts")) {
            turn = 0;
            System.out.printf("\n%s will start",player1.getName());;
        }else {
            turn = 1;
            System.out.printf("\n%s will start",player2.getName());;
        }

        //starting the game.
        while (!VictoryCondition.victory(gameBoard)||!FailureCondition.failure(gameBoard)) {
            round++;
            System.out.printf("\nRound %d", round);
            if (turn % 2 == 0){
                Display.displayBoard(gameBoard);
                System.out.printf("\n\n%S'S TURN", player1.getName());
                //launch the method that inputs the player's choice on the board
                gameBoard = Board.playerMove(gameBoard,player1);
                turn++;
                //checking if victory has been met
                if(VictoryCondition.victory(gameBoard)){
                    System.out.printf("\n VICTORY FOR %S", toUpperCase(player1.getName()));
                    Display.displayBoard(gameBoard);
                    System.out.println("\n\nCONGRATULATIONS !!!!");
                    break;
                }
            }else {
                Display.displayBoard(gameBoard);
                System.out.printf("\n\n%S'S TURN", player2.getName());
                //launch the method that inputs the player's choice on the board
                gameBoard = Board.playerMove(gameBoard,player2);
                turn++;
                //checking if victory has been met
                if(VictoryCondition.victory(gameBoard)){
                    System.out.printf("\n VICTORY FOR %S", toUpperCase(player2.getName()));
                    Display.displayBoard(gameBoard);
                    System.out.println("\n\nCONGRATULATIONS !!!!");
                    break;
                }
            //checking if board is not full with no winner
            }if(FailureCondition.failure(gameBoard)){
                Display.displayBoard(gameBoard);
                System.out.println("\n\nTHE GAME ENDED WITH NO WINNER");
                break;
            }
        }
        return gameBoard;
    }
}