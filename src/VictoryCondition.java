public class VictoryCondition {

    public static boolean victory(Board board) {
        char[][] tempBoard =  board.getBoard();

        if (tempBoard[0][2] == tempBoard[1][2] && tempBoard[1][2] == tempBoard[2][2] && tempBoard[0][2] != 95) {
            return true;
        }else if (tempBoard[0][1] == tempBoard[1][1] && tempBoard[1][1] == tempBoard[2][1] && tempBoard[0][1] != 95){
            return true;
        }else if (tempBoard[0][0] == tempBoard[1][0] && tempBoard[1][0] == tempBoard[2][0] && tempBoard[0][0] != 95) {
            return true;
        }else if (tempBoard[0][0] == tempBoard[0][1] && tempBoard[0][1] == tempBoard[0][2] && tempBoard[0][0] != 95){
            return true;
        }else if (tempBoard[1][0] == tempBoard[1][1] && tempBoard[1][1] == tempBoard[1][2] && tempBoard[1][0] != 95) {
            return true;
        }else if (tempBoard[2][0] == tempBoard[2][1] && tempBoard[2][1] == tempBoard[2][2] && tempBoard[2][0] != 95) {
            return true;
        }else if (tempBoard[0][0] == tempBoard[1][1] && tempBoard[1][1] == tempBoard[2][2] && tempBoard[0][0] != 95){
            return true;
        }else if (tempBoard[0][2] == tempBoard[1][1] && tempBoard[1][1] == tempBoard[2][0] && tempBoard[0][2] != 95){
            return true;
        } else return false;
    }
}
